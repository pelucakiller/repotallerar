

function gestionProductos() {
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products"; //?$orderby=UnitPrice&$top=5&$skip=3&$count=true" ;
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200 ) {
      //console.log(request.responseText );
      productosObtenidos = request.responseText;
      procesarProductos(productosObtenidos);
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarProductos(productos) {
   var json= JSON.parse(productos);

   var table = document.createElement("table");
   var tbody = document.createElement("tbody");
   table.classList.add("table");
   table.classList.add("table-striped");

   for (var i = 0; i<json.value.length; ++i) {
     var tr = document.createElement("tr");
     var tdName = document.createElement("td");
     var tdPrice = document.createElement("td");
     var tdStock = document.createElement("td");
      tdName.innerText = json.value[i].ProductName;
      tdPrice.innerText = json.value[i].UnitPrice;

      tdStock.innerText = json.value[i].UnitsInStock;
     tr.appendChild(tdName);
     tr.appendChild(tdPrice);
     tr.appendChild(tdStock);
     tbody.appendChild(tr)

     //console.log(json.value[i].ProductName);
   }
   table.appendChild(tbody);
   tablaProductos.appendChild(table);
}
