function gestionClientes() {
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers" ;
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200 ) {
      procesarClientes(request.responseText);
    }
  }
  request.open("GET", url, true);
  request.send();
}


function procesarClientes(clientes) {
   var json= JSON.parse(clientes);

   var table = document.createElement("table");
   var tbody = document.createElement("tbody");
   table.classList.add("table");
   table.classList.add("table-striped");

   for (var i = 0; i<json.value.length; ++i) {
     var tr = document.createElement("tr");
     var tdName = document.createElement("td");
     var tdCompany = document.createElement("td");
     var tdCountry = document.createElement("td");
     var flag = document.createElement("img");
      tdName.innerText = json.value[i].ContactName;
      tdCompany.innerText = json.value[i].CompanyName;
      var country = json.value[i].Country == "UK" ? "United-Kingdom" : json.value[i].Country;
      flag.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + country + ".png";
      flag.classList.add("flag");
     tr.appendChild(tdName);
     tr.appendChild(tdCompany);
     tdCountry.appendChild(flag);
     tr.appendChild(tdCountry);
     tbody.appendChild(tr)

     //console.log(json.value[i].ProductName);
   }
   table.appendChild(tbody);
   tablaClientes.appendChild(table);
}
